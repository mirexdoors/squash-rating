export const fetchHeaders = new Headers({
  Accept: 'application/json, text/plain',
  'Content-Type': 'application/x-www-form-urlencoded'
});
