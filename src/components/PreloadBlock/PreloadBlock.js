import React from 'react';
import './PreloadBlock.css';
import preloader from '../../assets/puff.svg';
const PreloadBlock = (props) => {
  let preloadingText = props.text ? props.text : 'Получение данных...'
  return (
    <div className={'preloadBlock ' + props.anotherClassName }>
      <div className="preloadBlock__content">
        <div className="preloadBlock__title">{preloadingText}</div>
        <div className="preloadBlock__imgContainer">
          <img src={preloader} className="preloadBlock__img" alt="загрузка..." />
        </div>
      </div>
    </div>
  )
};

export default PreloadBlock;