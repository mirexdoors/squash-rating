import React from 'react';
import './Blanket.css';

class Blanket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
  }


  componentWillReceiveProps(nextProps) {
    this.setState({ show: nextProps.show });
  }

  render() {
    if (!this.state.show) {
      return null;
    }

    return (
      <div className={`blanket blanket_${this.props.status}`}>
        <div className="blanket__content">
          <div className="blanket__title">{this.props.title}</div>
          <div className="blanket__description">{this.props.text}</div>
          <div className="blanket__btnContainer">
            <button
              className="button button_bgr"
              onClick={this.props.handleClose}
            >Ok
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Blanket;
