import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import PreloadBlock from '../PreloadBlock/PreloadBlock';
import PlayerDetailCard from '../PlayerDetailCard/PlayerDetailCard';
import { ApiPath } from '../../constants/ApiPath';


class Player extends Component {
  constructor(props) {
    super(props);
    this.handleUpdateMatchCount = this.handleUpdateMatchCount.bind(this);
    this.onHandleSearch = this.onHandleSearch.bind(this);

    this.state = {
      playerDetailData: { status: 'loading' },
      playerDetailInfo: {},
      playerMatchesHistory: {},
      playerMatchesHistoryCount: 10,
      playerID: props.params.player,
      showAllMatchBtn: true,
    };
  }

  componentDidMount() {
    this.loadData();

    browserHistory.listen((location) => {
      this.setState({
        playerID: location.pathname.split('/').pop(),
        playerDetailData: { status: 'loading' },
        playerMatchesHistoryCount: 10,
        showAllMatchBtn: true,
      }, () => this.loadData());
    });
  }

  onHandleSearch(event) {
    const re = new RegExp(event.target.value, 'i');
    const filteredPlayerMatchesHistory = this.state.playerDetailData.matches.filter(game => re.test(`${game.FIRSTNAME} ${game.LASTNAME}`));
    this.setState({
      playerMatchesHistory: filteredPlayerMatchesHistory
    });
  }

  handleUpdateMatchCount() {
    this.setState({
      playerMatchesHistoryCount: this.state.playerDetailData.matches.length,
      showAllMatchBtn: false
    });
  }

  loadData() {
    const myHeaders = new Headers({
    });

    const myInit = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(`${ApiPath}get.php?id=${this.state.playerID}`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError("Oops, we haven't got JSON!");
      })

      .then((data) => {
        console.log(data);
        this.setState({
          playerDetailData: data,
          playerDetailInfo: data.playerInfo[0],
          playerMatchesHistory: data.matches
        });
      })
      .catch((error) => {
        this.setState({ playerDetailData: { status: 'error' } });
      });
  }


  render() {
    if (this.state.playerDetailData.status === 'loading') {
      return <PreloadBlock />;
    }

    if (this.state.playerDetailData.status === 'error') {
      return <p>Ошибка получения данных с сервера: перезагрузите страницу.</p>;
    }

    return (
      <div>
        <PlayerDetailCard
          allPlayerMatches={this.state.playerDetailData.matches}
          playerData={this.state.playerDetailInfo}
          playerMatches={this.state.playerMatchesHistory}
          displayMatches={this.state.playerMatchesHistoryCount}
          onShowAllMatches={this.handleUpdateMatchCount}
          filterGames={this.onHandleSearch}
          showBtn={this.state.showAllMatchBtn}
          id={this.state.playerID}
          rating={this.props.rating}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    rating: state.rating,
  };
}

export default connect(mapStateToProps)(Player);
