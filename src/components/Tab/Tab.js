import React, { Component } from 'react';
import './Tab.css';

class Tab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  render() {
    return (
      <div className="tab">
        <div className="tab__wrap">
          {this.props.tabsData.map((item) => {
            let addClass = this.props.currentTab === item.tabvalue ? 'isCurrent' : '';
            return (
              <button
                className={'tab__button ' +  addClass}
                data-tabvalue={item.tabvalue}
                key={item.id}
                onClick={this.props.clickTab}
              >{item.text}</button>
            )
          })}
        </div>
      </div>
    )
  }
};

export default Tab;