import React from 'react';
import PropTypes from 'prop-types';

const AdminTableRow = ({
  name,
  lastName,
  dataKey,
  approveHandler,
  deleteHandler,
  refetchHandler,
}) => {
  const onApproveBtnPress = () => {
    if (window.confirm(`Действительно хотите подтвердить заявку игрока ${name} ${lastName}?`)) {
      approveHandler(dataKey, refetchHandler);
    }
  };

  const onDeleteBtnPress = () => {
    if (window.confirm(`Действительно хотите отклонить и удалить заявку игрока ${name} ${lastName}?`)) {
      deleteHandler(dataKey, refetchHandler);
    }
  };


  return (
    <tr>
      <td>
        <span>{name}</span>
      </td>
      <td>
        <span>{lastName}</span>
      </td>
      <td>
        <button
          data-id={dataKey}
          className="button button_brdr"
          onClick={() => onApproveBtnPress()}
        >
            Подтвердить
        </button>
      </td>
      <td>
        <button
          data-id={dataKey}
          className="button button_bgr button_bgr_color_red"
          onClick={() => onDeleteBtnPress()}
        >
          Удалить
        </button>
      </td>
    </tr>
  );
};

AdminTableRow.propTypes = {
  name: PropTypes.string,
  lastName: PropTypes.string,
  dataKey: PropTypes.number,
  approveHandler: PropTypes.func,
  deleteHandler: PropTypes.func,
  refetchHandler: PropTypes.func,
};

AdminTableRow.defaultProps = {
  name: 'Имя игрока',
  lastName: 'Фамилия игрока',
  dataKey: null,
};

export default AdminTableRow;
