import React from 'react';
import { Link } from 'react-router';
import { Chart } from 'react-google-charts';

import './PlayerDetailCard.css';

const PlayerDetailCard = ({
  playerData,
  playerMatches,
  displayMatches,
  allPlayerMatches,
  searchtext,
  filterGames,
  showBtn,
  onShowAllMatches,
  id,
  rating,
}) => {

  let playerMatchestoRender = playerMatches.slice(0, displayMatches);
  let chartData = [];

  allPlayerMatches.map((item, index) => {
    let chartItem = [];
    chartItem.push(++index);
    chartItem.push(Math.round(Number(item.RATE_AFTER)));
    chartItem.push(Math.round(Number(item.RATE_AFTER)) - Math.round(Number(item.RATE_BEFORE)));
    chartItem.push('Против ' + item.FIRSTNAME + ' ' + item.LASTNAME + ' со счетом: ' + item.SCORE);
    chartData.push(chartItem);
  });

  const getRatingPosition = function getRatingPosition() {
    if (!rating.rating) {
      return 'Общий рейтинг игроков ещё не загрузился...';
    }

    for (let i = 0; i < rating.rating.length; i++) {
      if (id === rating.rating[i].ID) {
        return rating.rating[i].place;
      }
    }
    return 'Нет рейтинга. Где-то закралась ошибка';
  }

  const placeInRating = getRatingPosition();

  const chartOptions = {
    lineWidth: 2,
    hAxis: {
      title: 'Игра'
    },
    vAxis: {
      title: 'Рейтинг'
    },
    theme: 'maximized',
    legend: 'none',
    series: {
      0: { color: '#333' },
    },
  };

  const columnsOptions = [
    {
      type: 'number',
      label: 'Игра',
    },
    {
      type: 'number',
      label: 'Рейтинг',
    },
    {
      label: null,
      type: 'number',
      role: 'annotation',
    },
    {
      label: null,
      type: 'string',
      role: 'annotationText',
    }
  ];

  return (
    <div className="playerDetailCard">
      <div className="playerDetailCard__wrap">
        <div className="playerDetailCard__data">
          <h1 className="playerDetailCard__name">
            {playerData.FIRSTNAME} {playerData.LASTNAME}
          </h1>
          <section className="playerDetailCard__section">
            <h2 className="playerDetailCard__topic">
              Общая статистика
            </h2>
            <div className="tableOuter">
              <div className="tableOuter__inner">
                <table className="table playerDetailCard__dataTable">
                  <tbody>
                    <tr>
                      <td>Место в рейтинге</td>
                      <td>{placeInRating}</td>
                    </tr>
                    <tr>
                      <td>Рейтинговые очки</td>
                      <td>{Math.floor(playerData.RATE)}</td>
                    </tr>
                    <tr>
                      <td>Всего матчей</td>
                      <td>{playerData.MATCHES}</td>
                    </tr>
                    <tr>
                      <td>Геймов выиграно / всего</td>
                      <td>{playerData.SETWIN} / {playerData.SETALL}</td>
                    </tr>
                    <tr>
                      <td>Дата последней игры</td>
                      <td>{playerData.LASTGAME.split('-').reverse().join('.')}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
          {allPlayerMatches[0] && false &&
            <section className="playerDetailCard__section">
              <h2 className="playerDetailCard__topic">
                История изменения рейтинга
              </h2>
              <div className="playerDetailCard__ratingChart">
                <Chart
                  chartType="ScatterChart"
                  options={chartOptions}
                  graph_id="Line"
                  width="100%"
                  height="400px"
                  columns={columnsOptions}
                  rows={chartData}
                />
              </div>
            </section>
          }

          {allPlayerMatches[0] &&
          <section className="playerDetailCard__section">
            <h2 className="playerDetailCard__topic">
              История матчей
            </h2>
            <div className="tableOuter">
              <div className="tableOuter__top">
                <input
                  type="search"
                  name="searchMatch"
                  className="input"
                  value={searchtext}
                  onChange={filterGames}
                  placeholder="Искать по соперникам..."
                />
              </div>
              <div className="tableOuter__inner">
                <table className="table playerDetailCard__matchTable">
                  <tbody>
                    {playerMatchestoRender.map((item) => {
                      let player1score = Number(item.SCORE.split('-')[0]);
                      let player2score = Number(item.SCORE.split('-')[1]);
                      let winnerID = player1score > player2score ? item['PLAYER 1 ID'] : item['PLAYER 2 ID'];

                    function LocalPlayer(anotherID) {
                      const winnerClass = id === winnerID ? 'winner' : 'loser';
                      return <td className={winnerClass}>{`${playerData.FIRSTNAME} ${playerData.LASTNAME}`}</td>;
                    }

                    function ContenderPlayer(anotherID) {
                      const winnerClass = anotherID === winnerID ? 'winner' : 'loser';
                      return <td className={winnerClass}><Link className="table__link" to={`/player/${anotherID}`}>{`${item.FIRSTNAME} ${item.LASTNAME}`}</Link></td>;
                    }

                    return (
                      <tr key={item.ID}>
                        {item['PLAYER 1 ID'] === id ? LocalPlayer() : ContenderPlayer(item['PLAYER 1 ID'])}
                        <td>{item.SCORE}</td>
                        {item['PLAYER 2 ID'] === id ? LocalPlayer() : ContenderPlayer(item['PLAYER 2 ID'])}
                        <td>{item.DATE.split('-').reverse().join('.')}</td>
                      </tr>
                    );
                  })}
                  </tbody>
                </table>
              </div>
              <div className="tableOuter__bottom">
                {showBtn &&
                  <button
                    className="button button_brdr"
                    onClick={onShowAllMatches}
                  >
                    Показать все матчи
                  </button>
                }
              </div>
            </div>
          </section>
          }
        </div>
      </div>
    </div>
  )
};

export default PlayerDetailCard;
