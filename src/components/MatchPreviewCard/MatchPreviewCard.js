import React from 'react';
import PropTypes from 'prop-types';

import './MatchPreviewCard.css';

const MatchPreviewCard = ({
  title,
  player1,
  player2,
  score,
  matchdate,
  onConfirm,
  onCancel,
}) => {
  const date = matchdate.split('-').reverse().join('.');

  return (
    <div className="matchPreviewCard">
      <div className="matchPreviewCard__wrap">
        <h2 className="matchPreviewCard__title">{title}</h2>
        <table className="matchPreviewCard__table">
          <tbody>
            <tr>
              <td>
                <span className="matchPreviewCard__playerName">{player1}</span>
              </td>
              <td>
                <span className="matchPreviewCard__score">{score}</span>
              </td>
              <td>
                <span className="matchPreviewCard__playerName">{player2}</span>
              </td>
            </tr>
            <tr>
              <td />
              <td>
                <span className="matchPreviewCard__date">{date}</span>
              </td>
              <td />
            </tr>
          </tbody>
        </table>
        <div className="matchPreviewCard__buttons">
          <button
            className="button button_bgr"
            onClick={onConfirm}
          >
            Всё верно, отправляем
          </button>
          <button
            className="button button_bgr button_bgr_color_red"
            onClick={onCancel}
          >
            Умоляю, не отправляйте
          </button>
        </div>
      </div>
    </div>
  );
};

MatchPreviewCard.propTypes = {
  title: PropTypes.string,
  player1: PropTypes.string,
  player2: PropTypes.string,
  score: PropTypes.string,
  matchdate: PropTypes.string,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

MatchPreviewCard.defaultProps = {
  title: 'Предпросмотр матча',
  player1: 'Игрок 1',
  player2: 'Игрок 2',
  score: '3:0',
  matchdate: '2018-03-19',
};

export default MatchPreviewCard;
