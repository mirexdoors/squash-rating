import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './RatingData.css';

import PreloadBlock from '../PreloadBlock/PreloadBlock';
import RatingTableRow from '../RatingTableRow/RatingTableRow';
import * as pageActions from '../../actions/rating';


class RatingData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dataToRender: false,
      searchtext: ''
    };
  }

  filterPlayers = (event) => {
    let query = event.target.value;
    query = query.split('\\').join('');
    const re = new RegExp(query, 'i');
    const searchResults = this.props.rating.rating.filter(player => re.test(`${player.FIRSTNAME} ${player.LASTNAME}`));
    this.setState({
      dataToRender: searchResults,
      searchtext: query
    });
  }

  render() {
    const datatoRenderNow = this.state.dataToRender ?
      this.state.dataToRender : this.props.rating.rating;

    if (this.props.rating.status === 'loading') {
      return (
        <PreloadBlock />
      );
    }

    if (this.props.rating.status === 'fail') {
      return <p>Ошибка получения данных с сервера: перезагрузите страницу.</p>;
    }

    return (
      <div>
        <div className="RatingData">
          <input
            type="search"
            name="searchPlayer"
            className="input"
            value={this.state.searchtext}
            onChange={this.filterPlayers}
            placeholder="Искать игрока..."
          />
          <table className="table table_mobile">
            <thead>
              <tr>
                <th>№</th>
                <th>Игрок</th>
                <th className="num">Рейтинг</th>
                <th className="num">Матчей всего</th>
                <th className="num">Последний матч</th>
                <th className="num">Геймов выиграно / всего</th>
              </tr>
            </thead>
            <tbody>
              {!!datatoRenderNow &&
                datatoRenderNow.map(item =>
                  <RatingTableRow data={item} key={item.place.toString()} />)
              }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    rating: state.rating,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RatingData);
