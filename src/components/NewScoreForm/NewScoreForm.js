import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';

import './NewScoreForm.css';

import { ApiPath } from '../../constants/ApiPath';
import { fetchHeaders } from '../../constants/fetch-headers';

import WarningHint from '../WarningHint/WarningHint';
import PreloadBlock from '../PreloadBlock/PreloadBlock';
import MatchPreviewCard from '../MatchPreviewCard/MatchPreviewCard';



class NewScoreForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // form
      currentStatus: 'base',
      showPreview: false,
      // fields values
      player1Value: '',
      player1ID: '',
      player2Value: '',
      player2ID: '',
      matchScoreValue: '3:0',
      matchDateTimeValue: '',
      // validation
      validationResult: true,
      player1ValidationStatus: false,
      player1ValidationText: false,
      player2ValidationStatus: false,
      player2ValidationText: false,
    };

    this.playersData = this.props.playersData;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validationFunc = this.validationFunc.bind(this);
    this.sendMatchData = this.sendMatchData.bind(this);
    this.refreshRating = this.refreshRating.bind(this);
  }

  refreshRating() {
    this.props.onSendMatchScore();
  }

  validationFunc() {
    let localValidationResult = true;
    let player1ValueCheck = false;
    let player2ValueCheck = false;

    this.props.playersData.forEach((item) => {
      if (`${item.FIRSTNAME} ${item.LASTNAME}` === this.state.player1Value) {
        player1ValueCheck = true;
        this.setState({
          player1ID: item.ID
        });
      }
    });

    this.props.playersData.forEach((item) => {
      if (`${item.FIRSTNAME} ${item.LASTNAME}` === this.state.player2Value) {
        player2ValueCheck = true;
        this.setState({
          player2ID: item.ID
        });
      }
    });

    if (player1ValueCheck) {
      this.setState({ player1ValidationStatus: false });
    } else {
      this.setState({ player1ValidationStatus: true });
      this.setState({ player1ValidationText: 'Этого игрока нет в нашей базе' });
      localValidationResult = false;
    }

    if (player2ValueCheck) {
      this.setState({ player2ValidationStatus: false });
    } else {
      this.setState({ player2ValidationStatus: true });
      this.setState({ player2ValidationText: 'Этого игрока нет в нашей базе' });
      localValidationResult = false;
    }

    if (this.state.player1Value === this.state.player2Value) {
      this.setState({ player1ValidationStatus: true });
      this.setState({ player2ValidationStatus: true });
      this.setState({ player1ValidationText: 'Игроки должны быть разными' });
      this.setState({ player2ValidationText: 'Игроки должны быть разными' });
      localValidationResult = false;
    }

    if (localValidationResult) {
      this.setState({ showPreview: true });
    }

    return localValidationResult;
  }

  handleChange(event) {
    switch (event.target.name) {
      case 'player1':
        this.setState({ player1Value: event.currentTarget.value });
        break;
      case 'player2':
        this.setState({ player2Value: event.currentTarget.value });
        break;
      case 'matchScore':
        this.setState({ matchScoreValue: event.currentTarget.value });
        break;
      case 'matchdate':
        this.setState({ matchDateTimeValue: event.currentTarget.value });
        break;
      default:
        console.warn('case for this field', event.target, ' is not defined');
        break;
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ validationResult: this.validationFunc() });
  }

  sendMatchData() {
    if (!this.state.validationResult) {
      return null;
    }
    this.setState({ currentStatus: 'sending' });
    let scoreCoef;

    switch (this.state.matchScoreValue) {
      case '3:0':
      case '0:3':
        scoreCoef = 30;
        break;
      case '3:1':
      case '1:3':
        scoreCoef = 20;
        break;
      case '3:2':
      case '2:3':
        scoreCoef = 10;
        break;
      default:
        console.warn('undexpected score');
        break;
    }

    const payload = {
      type: 'writescore',
      player1ID: Number(this.state.player1ID),
      player2ID: Number(this.state.player2ID),
      scorePlayer1: Number(this.state.matchScoreValue.split(':')[0].trim()),
      scorePlayer2: Number(this.state.matchScoreValue.split(':')[1].trim()),
      matchdate: this.state.matchDateTimeValue,
      n: scoreCoef
    };

    const myInit = {
      method: 'POST',
      headers: fetchHeaders,
      body: queryString.stringify(payload)
    };

    fetch(`${ApiPath}post.php`, myInit)
      .then((res) => {
        return res.json();
      }).then(() => {
        return this.refreshRating();
      }).then(() => {
        this.setState({
          currentStatus: 'success',
          showPreview: false,
          player1Value: '',
          player1ID: '',
          player2Value: '',
          player2ID: '',
          matchScoreValue: '3:0',
        }, () => {
          window.localStorage.setItem(`id=${this.state.player1ID}`, '');
          window.localStorage.setItem(`id=${this.state.player2ID}`, '');
          alert('Данные о матче внесены в базу');
        });
      })
      .catch((error) => {
        alert(error);
      });
  }

  render() {
    const players = this.props.playersData;

    return (

      <div className="newScoreForm">

        {this.state.currentStatus === 'sending' &&
          <PreloadBlock anotherClassName="newScoreForm__preloader" text="Запись данных матча..." />
        }

        {this.state.showPreview &&
          <MatchPreviewCard
            player1={this.state.player1Value}
            player2={this.state.player2Value}
            score={this.state.matchScoreValue}
            matchdate={this.state.matchDateTimeValue}
            onConfirm={this.sendMatchData}
            onCancel={() => this.setState({ showPreview: false })}
          />
        }

        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form__header form__header_inverse">
            <div className="form__wrap">
              <div className="form__title">Добавить результат матча</div>
            </div>
          </div>
          <div className="form__content">
            <div className="form__wrap">
              <div className="form__item">
                <div className="form__label">Игрок 1</div>
                <div className="form__itemContent">
                  <input
                    required
                    list="players"
                    className="input"
                    type="search"
                    name="player1"
                    onChange={this.handleChange}
                    style={{ maxWidth: `${350}px` }}
                    value={this.state.player1Value}
                  />

                  <WarningHint
                    warn={this.state.player1ValidationStatus}
                    warntext={this.state.player1ValidationText}
                  />

                  <datalist id="players">
                    {!!players && players.map((item, index) =>
                      <option value={`${item.FIRSTNAME} ${item.LASTNAME}`} key={item.place} />)}
                  </datalist>
                </div>
              </div>

              <div className="form__item">
                <div className="form__label">Игрок 2</div>
                <div className="form__itemContent">
                  <input
                    required
                    list="players"
                    className="input"
                    type="search"
                    name="player2"
                    onChange={this.handleChange}
                    style={{ maxWidth: `${350}px` }}
                    value={this.state.player2Value}
                  />

                  <WarningHint
                    warn={this.state.player2ValidationStatus}
                    warntext={this.state.player2ValidationText}
                  />

                </div>
              </div>

              <div className="form__item">
                <label className="form__label" htmlFor="matchScore">Счёт матча</label>
                <div className="form__itemContent">
                  <select
                    className="select"
                    id="matchScore"
                    name="matchScore"
                    onChange={this.handleChange}
                    value={this.state.matchScoreValue}
                  >
                    <option value="3:0">3:0</option>
                    <option value="3:1">3:1</option>
                    <option value="3:2">3:2</option>
                    <option value="2:3">2:3</option>
                    <option value="1:3">1:3</option>
                    <option value="0:3">0:3</option>
                  </select>
                </div>
              </div>

              <div className="form__item">
                <label className="form__label" htmlFor="matchdate">Дата матча</label>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="date"
                    id="matchdate"
                    name="matchdate"
                    onChange={this.handleChange}
                    style={{ maxWidth: `${240}px` }}
                    value={this.state.matchDateTimeValue}
                  />
                </div>
              </div>

              <div className="form__item">
                <button className="button button_bgr" type="submit">Отправить</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default NewScoreForm;

NewScoreForm.propTypes = {
  playersData: PropTypes.array.isRequired,
};
