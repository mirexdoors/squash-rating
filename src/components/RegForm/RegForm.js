import React from 'react';
import queryString from 'query-string';
import Blanket from '../Blanket/Blanket';
import PreloadBlock from '../PreloadBlock/PreloadBlock';
import { ApiPath } from '../../constants/ApiPath';

class RegForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.prevalidation = this.prevalidation.bind(this);
    this.sendRegData = this.sendRegData.bind(this);
    this.onBlanketClose = this.onBlanketClose.bind(this);

    this.state = {
      // form
      currentStatus: 'base',
      // fields values
      emailValue: '',
      nameValue: '',
      lastnameValue: '',
      policyValue: true,
      // validation
      filledFields: false,
      // blanket
      blanketShow: false,
      blanketTitle: 'blanketTitle',
      blanketText: 'blanketText',
      blanketStatus: 'default'
    };
  }

  onBlanketClose() {
    this.setState({
      blanketShow: false,
      blanketTitle: 'blanketTitle',
      blanketText: 'blanketText',
      blanketStatus: 'default'
    });
  }

  handleChange(event) {
    let newstate = {};
    switch (event.target.name) {
      case 'email':
        newstate = { emailValue: event.currentTarget.value };
        break;
      case 'name':
        newstate = { nameValue: event.currentTarget.value };
        break;
      case 'lastname':
        newstate = { lastnameValue: event.currentTarget.value };
        break;
      case 'policy':
        newstate = { policyValue: event.target.checked };
        break;
      default:
        console.warn('case for this field', event.target, ' is not defined');
        break;
    }
    this.setState(newstate, () => {
      this.prevalidation();
    });
  }

  prevalidation() {
    if (this.state.emailValue && this.state.nameValue && this.state.lastnameValue && this.state.policyValue) {
      this.setState({ filledFields: true });
    } else {
      this.setState({ filledFields: false });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.sendRegData();
  }

  sendRegData() {
    this.setState({ currentStatus: 'sending' });

    const payload = {
      type: 'reg',
      email: this.state.emailValue,
      name: this.state.nameValue,
      lastname: this.state.lastnameValue
    };

    const myHeaders = new Headers({
      Accept: 'application/json, text/plain',
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const myInit = {
      method: 'POST',
      headers: myHeaders,
      credentials: 'include',
      body: queryString.stringify(payload)
    };

    fetch(`${ApiPath}post.php`, myInit)
      .then((res) => {
        return res.json();
      }).then((data) => {
        if (data.status === false) {
          const newstate = {
            blanketStatus: 'fail',
            currentStatus: 'fail',
            blanketShow: true
          };

          switch (data.msg) {
            case 'not approve':
              newstate.blanketTitle = 'Ваша заявка ещё не одобрена';
              newstate.blanketText = 'Ваша заявка находится на рассмотрении. Пожалуйста, дождитесь пока её одобрят или свяжитесь с администрацией.';
              break;
            case 'same name and lastname':
              newstate.blanketTitle = 'Игрок с таким именем и фамилией уже существует';
              newstate.blanketText = 'К сожалению, такой игрок уже есть. Попробуйте написать имя / фамилию в верхнем регистре.';
              break;
            case 'already registered and approved':
              newstate.blanketTitle = 'Вы уже зарегистрированы';
              newstate.blanketText = 'Игрок с таким email адресом уже зарегистрирован на сайте. Если вы забыли пароль, передите во вкладу «Восстановление пароля»';
              break;
          }
          this.setState(newstate);
        }
        if (data.status === true) {
          const newstate = {
            blanketStatus: 'success',
            currentStatus: 'success',
            blanketTitle: 'Заявка принята, но не активирована',
            blanketText: 'Аккаунт игрока будет активирован после одобрения администратором. Когда это случится, вам на указанный email придёт логин и пароль.',
            blanketShow: true
          };
          this.setState(newstate);
        }
      }).catch((error) => {
        alert(error);
      });
  }

  render() {
    return (
      <div className="loginForm">

        {this.state.currentStatus === 'sending' &&
          <PreloadBlock anotherClassName="preloadBlock_overlay" text="Отправка данных..." />
        }

        <Blanket
          show={this.state.blanketShow}
          title={this.state.blanketTitle}
          text={this.state.blanketText}
          status={this.state.blanketStatus}
          handleClose={this.onBlanketClose}
        />

        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form__header form__header_green">
            <div className="form__wrap">
              <div className="form__title">Регистрация</div>
            </div>
          </div>
          <div className="form__content">
            <div className="form__wrap">
              <div className="form__item">
                <div className="form__label">Email</div>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="email"
                    pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
                    name="email"
                    value={this.state.emailValue}
                    style={{ maxWidth: `${350}px` }}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="form__item">
                <div className="form__label">Имя</div>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="text"
                    name="name"
                    style={{ maxWidth: `${350}px` }}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="form__item">
                <div className="form__label">Фамилия</div>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="text"
                    name="lastname"
                    style={{ maxWidth: `${350}px` }}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="form__item">
                <div>
                  <p>После подтверждения заявки, пароль будет выслан вам на почту</p>
                </div>
                <button
                  className="button button_bgr"
                  type="submit"
                  disabled={!this.state.filledFields}
                >Зарегистрироваться
                </button>
              </div>

              <div className="form__item">
                <label onChange={this.handleChange}>
                  <input
                    required
                    defaultChecked={this.state.policyValue}
                    name="policy"
                    type="checkbox"
                  />
                  <span>согласен(на) с политикой обработки персональных данных</span>
                </label>
              </div>

            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default RegForm;
