import React from 'react';
import { Link } from 'react-router';
import './Menu.css';


const Menu = ({ userinfo, unLogin }) => {
  let isUserWithSpecialStatus = false;

  if (userinfo.status === 'playerskiy' || userinfo.status === 'adminskiy') {
    isUserWithSpecialStatus = true;
  }

  return (
    <div className="menu">
      <div className="baseWrap">
        <ul className="menu__list">
          <li><Link onlyActiveOnIndex activeClassName="isCurrent" to="/">Главная</Link></li>
          {userinfo.status === 'simple' &&
            <li><Link activeClassName="isCurrent" to="/login">Логин</Link></li>
          }
          {isUserWithSpecialStatus && false &&
            <li><Link activeClassName="isCurrent" to="/lk">Личный кабинет</Link></li>
          }
          {userinfo.status === 'adminskiy' &&
            <li><Link activeClassName="isCurrent" to="/admin">Админка</Link></li>
          }
        </ul>
        <div className="menu__statusBar">
          <span>Вы <span className="menu__username">{userinfo.name}</span> </span>
          {isUserWithSpecialStatus &&
            <span><button onClick={unLogin}>Выйти</button></span>
          }
        </div>
      </div>
    </div>
  );
};

export default Menu;
