import React from 'react';
import './WarningHint.css';

const WarningHint = (props) => {
  if (!props.warn) {
    return null;
  }

  let warningText = props.warntext ? props.warntext : 'Поле заполнено некорректно';

  return (
    <div className="warningHint">
      <div className="warningHint__wrap">
        <span className="warningHint__text"> {warningText} </span>
      </div>
    </div>
  )
};

export default WarningHint;