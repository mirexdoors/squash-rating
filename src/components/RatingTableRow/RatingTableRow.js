import React from 'react';
import { Link } from 'react-router';

const RatingTableRow = (props) => {
  const rowData = props.data;

  return (
    <tr>
      <td className="ratingOrder">
        <span className="table__cellData">{rowData.place}</span>
      </td>

      <td>
        <span className="table__cellHint">Имя:</span>
        <span className="table__cellData">
          <Link className="table__link" to={`/player/${rowData.ID}`}>
            {rowData.FIRSTNAME} {rowData.LASTNAME}
          </Link>
        </span>
      </td>

      <td className="num">
        <span className="table__cellHint">Рейтинг:</span>
        <span className="table__cellData">{Math.floor(rowData.RATE)}</span>
      </td>

      <td className="num noMobile">
        <span className="table__cellHint">Матчей всего:</span>
        <span className="table__cellData">{rowData.MATCHES}</span>
      </td>

      <td className="num noMobile">
        <span className="table__cellHint">Последний матч:</span>
        <span className="table__cellData">{rowData.LASTGAME.split('-').reverse().join('.')}</span>
      </td>

      <td className="num noMobile">
        <span className="table__cellHint">Геймов выиграно / всего:</span>
        <span className="table__cellData">{rowData.SETWIN} / {rowData.SETALL}</span>
      </td>
    </tr>
  );
};

export default RatingTableRow;
