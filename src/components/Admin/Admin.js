import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as adminActions from '../../actions/admin';
import * as ratingActions from '../../actions/rating';

import NewScoreForm from '../NewScoreForm/NewScoreForm';
import PreloadBlock from '../PreloadBlock/PreloadBlock';
import AdminTableRow from '../AdminTableRow/AdminTableRow';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.fetchRatingData = this.props.ratingActions.fetchRatingData.bind(this);
    this.fetchApplicants = this.props.adminActions.fetchApplicantsData.bind(this);
  }

  componentWillMount() {
    if (this.props.user.status !== 'adminskiy') {
      return false;
    }
    this.fetchApplicants();
  }

  render() {
    const { status, applicants } = this.props.admin;
    const { rating } = this.props.rating;
    const { approveApplicantRequest, deleteApplicant, fetchApplicantsData } = this.props.adminActions;

    if (status === 'loading' || !this.props.admin) {
      return <PreloadBlock />;
    }

    if (status === 'error') {
      return <p>Ошибка получения данных с сервера: {this.props.admin.status} <br /> перезагрузите страницу.</p>;
    }

    if (this.props.user.status !== 'adminskiy') {
      return null;
    }

    return (
      <div>
        <h1>Добавление матчей</h1>
        {!!rating &&
          <NewScoreForm
            playersData={rating}
            onSendMatchScore={this.fetchRatingData}
          />
        }
        <h1>Текущие заявки</h1>
        <div className="table__outer">
          <table className="table">
            <thead>
              <tr>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Подтвердить</th>
                <th>Отклонить и удалить</th>
              </tr>
            </thead>
            <tbody>
              {!!applicants && applicants.map((item) => {
                return (
                  <AdminTableRow
                    key={item.ID}
                    dataKey={Number(item.ID)}
                    name={item.FIRSTNAME}
                    lastName={item.LASTNAME}
                    approveHandler={approveApplicantRequest}
                    deleteHandler={deleteApplicant}
                    refetchHandler={fetchApplicantsData}
                  />
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    admin: state.admin,
    rating: state.rating,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    adminActions: bindActionCreators(adminActions, dispatch),
    ratingActions: bindActionCreators(ratingActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
