import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import './LoginForm.css';

import Blanket from '../Blanket/Blanket';
import PreloadBlock from '../PreloadBlock/PreloadBlock';
import { ApiPath } from '../../constants/ApiPath';
import * as userActions from '../../actions/user';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.prevalidation = this.prevalidation.bind(this);
    this.sendLoginData = this.sendLoginData.bind(this);
    this.onBlanketClose = this.onBlanketClose.bind(this);

    this.state = {
      // form
      currentStatus: 'base',
      // fields values
      emailValue: '',
      passwordValue: '',

      // validation
      filledFields: false,

      // blanket
      blanketShow: false,
      blanketTitle: 'blanketTitle',
      blanketText: 'blanketText',
      blanketStatus: 'default'
    };
  }

  onBlanketClose() {
    this.setState({
      blanketShow: false,
      blanketTitle: 'blanketTitle',
      blanketText: 'blanketText',
      blanketStatus: 'default'
    });
  }

  handleChange(event) {
    let newstate = {};
    switch (event.target.name) {
      case 'email':
        newstate = { emailValue: event.target.value };
        break;
      case 'password':
        newstate = { passwordValue: event.target.value };
        break;
      default:
        console.warn('case for this field', event.target, ' is not defined');
        break;
    }
    this.setState(newstate, () => {
      this.prevalidation();
    });
  }

  prevalidation() {
    if (this.state.emailValue && this.state.passwordValue) {
      this.setState({ filledFields: true });
    } else {
      this.setState({ filledFields: false });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.sendLoginData();
  }

  sendLoginData() {
    this.setState({ currentStatus: 'sending' });

    const payload = {
      type: 'login',
      email: this.state.emailValue,
      password: this.state.passwordValue,
    };

    const myHeaders = new Headers({
      Accept: 'application/json, text/plain',
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const myInit = {
      method: 'POST',
      headers: myHeaders,
      credentials: 'include',
      body: queryString.stringify(payload)
    };

    fetch(`${ApiPath}post.php`, myInit)
      .then((res) => {
        return res.json();
      }).then((data) => {
        if (data.status === false) {
          const newstate = {
            blanketStatus: 'fail',
            currentStatus: 'fail',
            blanketShow: true
          };

          switch (data.msg) {
            case 'No user':
              newstate.blanketTitle = 'Такого пользователя нет';
              newstate.blanketText = 'Пользователя с таким email нет в нашей базе данных, пожалуйста, проверьте корректность email или зарегистрируйтесь';
              break;
            case 'Bad password':
              newstate.blanketTitle = 'Неверный пароль';
              newstate.blanketText = 'Вы ввели неверный, пожалуйста, проверьте его корректность, возможно, лишний пробел?';
              break;
          }

          this.setState(newstate);
        }

        if (data.status === true) {
          console.log(data);
          const { loginUser } = this.props.userActions;
          loginUser(data);
          const newstate = {
            blanketStatus: 'success',
            currentStatus: 'success',
            blanketTitle: 'Вы авторизированы!',
            blanketText: 'Приветствуем, вы успешно авторизировались',
            blanketShow: true
          };

          this.setState(newstate, () => {
            window.localStorage.setItem('userstatus', 'player');
          });
        }
      }).catch((error) => {
        alert(error);
      });
  }

  render() {
    return (
      <div className="loginForm">

        {this.state.currentStatus === 'sending' &&
          <PreloadBlock anotherClassName="preloadBlock_overlay" text="Авторизация..." />
        }

        <Blanket
          show={this.state.blanketShow}
          title={this.state.blanketTitle}
          text={this.state.blanketText}
          status={this.state.blanketStatus}
          handleClose={this.onBlanketClose}
        />

        <form className="form" onSubmit={this.handleSubmit} autoComplete="on">
          <div className="form__header form__header_yellow">
            <div className="form__wrap">
              <div className="form__title">Вход</div>
            </div>
          </div>
          <div className="form__content">
            <div className="form__wrap">
              <div className="form__item">
                <div className="form__label">Email</div>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="email"
                    pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
                    name="email"
                    autoComplete="on"
                    style={{ maxWidth: `${350}px` }}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="form__item">
                <div className="form__label">Пароль</div>
                <div className="form__itemContent">
                  <input
                    required
                    className="input"
                    type="password"
                    name="password"
                    autoComplete="current-password"
                    style={{ maxWidth: `${350}px` }}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="form__item">
                <button
                  className="button button_bgr"
                  type="submit"
                  disabled={!this.state.filledFields}
                >Войти
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
