import React, { Component } from 'react';
import LoginForm from '../LoginForm/LoginForm';
import RegForm from '../RegForm/RegForm';
import Tab from '../Tab/Tab';

class Login extends Component {
  constructor(props) {
    super(props);
    this.onHandleTab = this.onHandleTab.bind(this);

    this.state = {
      showTab: 'login',
      tabs: [
        {
          id: 1,
          text: 'Вход',
          tabvalue: 'login'
        },
        {
          id: 2,
          text: 'Регистрация',
          tabvalue: 'reg'
        }
      ]
    };
  }

  onHandleTab(event) {
    this.setState({
      showTab: event.target.getAttribute('data-tabvalue')
    });
  }

  render() {
    return (
      <div>
        <Tab
          tabsData={this.state.tabs}
          clickTab={this.onHandleTab}
          currentTab={this.state.showTab}
        />
        {this.state.showTab === 'login' &&
          <LoginForm />
        }
        {this.state.showTab === 'reg' &&
          <RegForm />
        }
      </div>
    );
  }
}

export default Login;
