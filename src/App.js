import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './App.css';

import logoPic from'./assets/logo.png';
import Menu from './components/Menu/Menu';

import * as userActions from './actions/user';
import * as pageActions from './actions/rating';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentWillMount() {
    this.props.userActions.prelogin();
    this.props.pageActions.fetchRatingData();
  }

  render() {
    const { user, children, userActions } = this.props;
    return (
      <div className="App">
        <header className="App__header">
          <div className="baseWrap">
            <h1 className="App__title">Рейтинг Новосибирской Федерации сквоша</h1>
            <a href="http://sibsquash.ru/" rel="noopener noreferrer" target="_blank" className="logo">
              <img className="logo__img" src={logoPic} alt="" />
            </a>
          </div>
          <Menu
            userinfo={user}
            unLogin={userActions.unloginUser}
          />
        </header>
        <div className="App__content">
          <div className="baseWrap">
            {children}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    rating: state.rating,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    pageActions: bindActionCreators(pageActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
