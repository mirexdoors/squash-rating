import queryString from 'query-string';

import adminConstants from '../constants/admin';
import { ApiPath } from '../constants/ApiPath';

const {
  REQUEST_APPLICANTS,
  FETCH_APPLICANTS,
  FAIL_FETCH_APPLICANTS,
  SEND_APPLICANTS_STATUS,
  SUCCES_SEND_APPLICANTS_STATUS,
  FAIL_SEND_APPLICANTS_STATUS,
  SEND_APPLICANTS_DEL,
  SUCCES_APPLICANTS_DEL,
  FAIL_APPLICANTS_DEL,
} = adminConstants;

export function fetchApplicantsData() {
  return (dispatch) => {
    const myHeaders = new Headers({});

    const myInit = {
      method: 'GET',
      headers: myHeaders,
      credentials: 'include',
    };

    dispatch({
      type: REQUEST_APPLICANTS,
    });

    fetch(`${ApiPath}get.php?reg_status`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');

        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError('Oops, we have not got JSON!');
      })
      .then((data) => {
        let recivedData = data;
        if (recivedData.status === false) {
          recivedData = false;
        }
        dispatch({
          type: FETCH_APPLICANTS,
          payload: recivedData,
        });
      })
      .catch((error) => {
        dispatch({
          type: FAIL_FETCH_APPLICANTS,
          payload: error,
        });
      });
  };
}

export function deleteApplicant(playerID, onSuccess) {
  return (dispatch) => {
    const myHeaders = new Headers({
      Accept: 'application/json, text/plain',
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    const payload = {
      type: 'delete',
      id: playerID,
    };

    const myInit = {
      method: 'POST',
      headers: myHeaders,
      credentials: 'include',
      body: queryString.stringify(payload)
    };

    dispatch({
      type: SEND_APPLICANTS_DEL,
    });

    fetch(`${ApiPath}post.php`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');

        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError('Oops, we have not got JSON!');
      })
      .then((data) => {
        dispatch({
          type: SUCCES_APPLICANTS_DEL,
          payload: data,
        });
        onSuccess();
      })
      .catch((error) => {
        dispatch({
          type: FAIL_APPLICANTS_DEL,
          payload: error,
        });
      });
  };
}

export function approveApplicantRequest(playerID, onSuccess) {
  return (dispatch) => {
    const myHeaders = new Headers({
      Accept: 'application/json, text/plain',
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    const payload = {
      type: 'change_reg_status',
      id: playerID,
    };

    const myInit = {
      method: 'POST',
      headers: myHeaders,
      credentials: 'include',
      body: queryString.stringify(payload)
    };

    dispatch({
      type: SEND_APPLICANTS_STATUS,
    });

    fetch(`${ApiPath}post.php`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');

        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError('Oops, we have not got JSON!');
      })
      .then((data) => {
        dispatch({
          type: SUCCES_SEND_APPLICANTS_STATUS,
          payload: data,
        });
        onSuccess();
      })
      .catch((error) => {
        dispatch({
          type: FAIL_SEND_APPLICANTS_STATUS,
          payload: error,
        });
      });
  };
}
