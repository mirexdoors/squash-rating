import ratingConstants from '../constants/rating';
import { ApiPath } from '../constants/ApiPath';

const {
  REQUEST_RATING,
  FETCH_RATING,
  FAIL_FETCH_RATING,
} = ratingConstants;

export function fetchRatingData() {
  return (dispatch) => {
    const myHeaders = new Headers({});

    const myInit = {
      method: 'GET',
      credentials: 'include',
      headers: myHeaders,
    };

    dispatch({
      type: REQUEST_RATING,
      payload: myInit
    });

    fetch(`${ApiPath}get.php?q=1`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError("Oops, we haven't got JSON!");
      })
      .then((playersStats) => {
        playersStats.sort((a, b) => { return (Number(b.RATE) - Number(a.RATE)); });
        playersStats.map((item, index) => item.place = index + 1);

        dispatch({
          type: FETCH_RATING,
          payload: playersStats,
        });
      })
      .catch((error) => {
        dispatch({
          type: FAIL_FETCH_RATING,
          payload: error,
        });
      });
  };
}
