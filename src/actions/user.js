import queryString from 'query-string';

import userConstants from '../constants/user';
import { fetchHeaders } from '../constants/fetch-headers';
import { ApiPath } from '../constants/ApiPath';

const {
  LOGIN_SUCCESS,
  UNLOGIN_SUCCESS
} = userConstants;


export function prelogin() {
  return (dispatch) => {
    const payload = {
      type: 'user_status',
    };
    const myInit = {
      method: 'POST',
      headers: fetchHeaders,
      credentials: 'include',
      body: queryString.stringify(payload)
    };

    fetch(`${ApiPath}post.php`, myInit)
      .then((response) => {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
          return response.json();
        }
        throw new TypeError('Oops, we have not got JSON!');
      })
      .then((res) => {
        if (res.status === true) {
          const userPayload = {};
          if (res.user_data.REG_STATUS === 'true') {
            userPayload.userstatus = 'playerskiy';
          } else if (res.user_data.REG_STATUS === 'super') {
            userPayload.userstatus = 'adminskiy';
          }
          userPayload.id = res.user_data.ID;
          userPayload.username = `${res.user_data.FIRSTNAME} ${res.user_data.LASTNAME}`;
          console.log('userPayload:', userPayload);
          dispatch({
            type: LOGIN_SUCCESS,
            payload: userPayload
          });
        }
      }).catch((error) => {
        alert(error);
      });
  };
}


export function loginUser(userdata) {
  return {
    type: LOGIN_SUCCESS,
    payload: userdata
  };
}

export function unloginUser() {
  return {
    type: UNLOGIN_SUCCESS,
  };
}
