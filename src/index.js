import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import './index.css';
import App from './App';
import configureStore from './store/configureStore';
import Admin from './components/Admin/Admin';
import Player from './components/Player/Player';
import RatingData from './components/RatingData/RatingData';
import Login from './components/Login/Login';
import NotFound from './components/NotFound/NotFound';

// import registerServiceWorker from './registerServiceWorker';
// registerServiceWorker();

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={RatingData} />
        <Route path="login" component={Login} />
        <Route path="admin" component={Admin} />
        <Route path="player/:player" component={Player} />
        <Route path="*" component={NotFound} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
