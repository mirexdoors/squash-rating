import adminConstants from '../constants/admin';

const {
  REQUEST_APPLICANTS,
  FETCH_APPLICANTS,
  FAIL_FETCH_APPLICANTS,
  SEND_APPLICANTS_STATUS,
  SUCCES_SEND_APPLICANTS_STATUS,
  FAIL_SEND_APPLICANTS_STATUS,
  SEND_APPLICANTS_DEL,
  SUCCES_APPLICANTS_DEL,
  FAIL_APPLICANTS_DEL,
} = adminConstants;

const initialState = {
  userChangeStatusProgress: 'initial',
  status: 'initial',
  applicants: null,
};

export default function admin(state = initialState, action) {
  switch (action.type) {
    case REQUEST_APPLICANTS:
      return {
        ...state,
        status: 'loading',
      };

    case FETCH_APPLICANTS:
      return {
        ...state,
        status: 'success',
        applicants: action.payload,
      };

    case FAIL_FETCH_APPLICANTS:
      return {
        ...state,
        status: 'fail',
        error: action.payload,
      };

    case SEND_APPLICANTS_STATUS:
      return {
        ...state,
        userChangeStatusProgress: 'loading',
      };

    case SUCCES_SEND_APPLICANTS_STATUS:
      return {
        ...state,
        userChangeStatusProgress: 'success',
      };

    case FAIL_SEND_APPLICANTS_STATUS:
      return {
        ...state,
        userChangeStatusProgress: 'fail',
      };

    case SEND_APPLICANTS_DEL:
      return {
        ...state,
        userChangeStatusProgress: 'loading',
      };

    case SUCCES_APPLICANTS_DEL:
      return {
        ...state,
        userChangeStatusProgress: 'success',
      };

    case FAIL_APPLICANTS_DEL:
      return {
        ...state,
        userChangeStatusProgress: 'fail',
      };

    default:
      return state;
  }
}
