import { combineReducers } from 'redux';
import rating from './rating';
import user from './user';
import admin from './admin';

export default combineReducers({
  rating,
  user,
  admin,
});
