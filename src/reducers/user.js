import userConstants from '../constants/user';

const {
  LOGIN_SUCCESS,
  UNLOGIN_SUCCESS
} = userConstants;

const initialState = {
  name: 'Неопознанный енот',
  status: 'simple',
  id: null,

  // id: '5',
  // name: 'Dr. Джон Зойдберг',
  // status: 'adminskiy',
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        status: action.payload.userstatus,
        name: action.payload.username,
        id: action.payload.id,
      };

    case UNLOGIN_SUCCESS:
      return {
        ...state,
        name: 'Неопознанный енот',
        status: 'simple',
        id: null,
      };

    default:
      return state;
  }
}
