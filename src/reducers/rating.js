import ratingConstants from '../constants/rating'

const {
  REQUEST_RATING,
  FETCH_RATING,
  FAIL_FETCH_RATING,
  FILTER_RATING
} = ratingConstants;

const initialState = {
  rating: null,
  status: 'loading'
};

export default function rating(state = initialState, action) {
  switch (action.type) {
    case REQUEST_RATING:
      return { 
        ...state, 
        status: 'loading',
        headers: action.payload 
      }

    case FETCH_RATING:
      return { 
        ...state, 
        status: 'success',
        rating: action.payload 
      }

    case FAIL_FETCH_RATING:
      return { 
        ...state, 
        status: 'fail',
        error: action.payload 
      }

    case FILTER_RATING:
      return { 
        ...state, 
        filteredRating: action.payload 
      }
      
    default:
      return state;
  }
}