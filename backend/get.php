<?session_start();
require_once('bd_access.php');
require_once('modules/headers.php');
require_once('modules/db_pdo.php');

#выборка из таблицы players
include('modules/get/players_selection.php');

#незарегистрированные игроки
include('modules/get/unreg_players.php');

// Получение данных об игроке
include('modules/get/player_info.php');
?>