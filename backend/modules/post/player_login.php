<?
if ($_POST["type"] == "login") {

    // извлечение данных
 $email = $_POST['email'];
 $password = $_POST['password'];

 $sql = $pdo->prepare("SELECT EMAIL, ID, PASSWORD, REG_STATUS, FIRSTNAME, LASTNAME FROM players WHERE EMAIL = '$email'");
 $sql->execute();
 $playerData = $sql->fetchAll();


 if (empty($playerData)) {
  $answerArr = array('status' => false, 'msg' => 'No user');
} else {
  $hashPass = $playerData[0]['PASSWORD'];
  if (password_verify($password, $hashPass) && $playerData[0]['REG_STATUS'] == 'true') {
    $username = $playerData[0]['FIRSTNAME'].' '.$playerData[0]['LASTNAME'];
    $userId = $playerData[0]['ID'];
   
    $_SESSION['user'] = $userId;
    $sessionId = session_id();
    $answerArr = array('status' => true, 'userstatus' => 'playerskiy', 'id' => $sessionId, 'username' => $username, 'msg' => 'Success');

    
    
  } elseif (password_verify($password, $hashPass) && $playerData[0]['REG_STATUS'] == 'super') {
    $username = $playerData[0]['FIRSTNAME'].' '.$playerData[0]['LASTNAME'];
    $userId = $playerData[0]['ID'];
    $answerArr = array('status' => true, 'userstatus' => 'adminskiy', 'id' => $userId, 'username' => $username, 'msg' => 'Success');
    //session_start();
    $_SESSION['user'] = $userId;
  }elseif (password_verify($password, $hashPass) && empty($playerData[0]['REG_STATUS'])) {
    $answerArr = array('status' => false, 'msg' => 'User isn`t approved');
  } else {
    $answerArr = array('status' => false, 'msg' => 'Bad password');
  }
}

echo json_encode($answerArr);

}
?>
