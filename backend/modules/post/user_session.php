<?
if ($_POST["type"] == "user_status") {

	if (isset($_SESSION['user'])) {
		$userId = $_SESSION['user'];
		$sql = $pdo->prepare("SELECT ID, FIRSTNAME, LASTNAME, REG_STATUS FROM players WHERE ID = '$userId'");
		$sql->execute();
		$userData = $sql->fetchAll();

		if (!empty($userData)) {
			if ($userData[0]['REG_STATUS'] == 'true' || $userData[0]['REG_STATUS'] == 'super') {
				$answerArr = array('status' => true, 'user_data' => $userData[0]);
			} else {
				$answerArr = array('status' => false, 'msg' => 'User is not approved');
			}
		} else {
			$answerArr = array('status' => false, 'msg' => 'No user with this ID');
		}
	} else {

		$answerArr = array('status' => false, 'msg' => 'session is empty');
	}

   // пишем результат на фронт
	echo json_encode($answerArr);
}
?>
