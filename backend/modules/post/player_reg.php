<?
if ($_POST["type"] === "reg") {

    // извлечение данных
    $email = $_POST['email'];
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $regStatus = 'false'; // статус true получает после одобрения админом

    $sql = $pdo->prepare("SELECT `REG_STATUS`, `EMAIL`, `FIRSTNAME`, `LASTNAME` FROM players");
    $sql->execute();
    $allPlayersData = $sql->fetchAll();

    // проверяем есть ли уже такой email в базе

    $sameEmailKey = array_search($email, array_column($allPlayersData, 'EMAIL'));


    if ($sameEmailKey && $allPlayersData[$sameEmailKey]['REG_STATUS'] === 'false') {
    $answerArr = array('status' => false, 'msg' => 'not approve');
    echo json_encode($answerArr);
       die();
    }

    if ($sameEmailKey && $allPlayersData[$sameEmailKey]['REG_STATUS'] === 'true') {
    $answerArr = array('status' => false, 'msg' => 'already registered and approved');
    echo json_encode($answerArr);
        die();
    }

    // проверяем, есть ли уже такое сочетание ИМЯ + ФАМИЛИЯ в базе

    $sameLastNameKey = array_search($lastname, array_column($allPlayersData, 'LASTNAME'));

    if ($sameLastNameKey && $allPlayersData[$sameLastNameKey]['FIRSTNAME'] === $name) {
    $answerArr = array('status' => false, 'msg' => 'same name and lastname');
    echo json_encode($answerArr);
        die();
    }

  $defaultRating = 1600;

    // пишем в базу данных
    $sql = $pdo->prepare("INSERT INTO players (`FIRSTNAME`,`LASTNAME`,`EMAIL`,`REG_STATUS`, `RATE`) values ('$name', '$lastname', '$email', '$regStatus', '$defaultRating')");
    $sql->execute();

    // пишем результат на фронт
    $answerArr = array('status' => true, 'msg' => 'Success');
    echo json_encode($answerArr);

}
?>
