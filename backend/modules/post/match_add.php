<?
if ($_POST["type"] === "writescore") {
    // извлечение данных
    $player1ID = $_POST['player1ID'];
    $player2ID = $_POST['player2ID']; 
    $scorePlayer1 = $_POST['scorePlayer1'];
    $scorePlayer2 = $_POST['scorePlayer2'];
    $matchDate =  $_POST['matchdate'];
    $N = $_POST['n'];
    $score = $scorePlayer1 . " - " . $scorePlayer2;

    //обновление рейтинга
    $sqlUpdate ="SELECT ID, RATE, MATCHES, SETWIN, SETALL FROM players WHERE ID=$player1ID OR ID=$player2ID";
    $resultUpdate = $pdo->prepare($sqlUpdate);
    $resultUpdate->execute();
    $updateRateArray = $resultUpdate->fetchAll();

    $setInMatch = $scorePlayer1 + $scorePlayer2 ; //сетов сыграно в матче
    if ($updateRateArray[0]['ID'] == $player1ID) {
      $player1Arr = $updateRateArray[0];
      $player2Arr = $updateRateArray[1];
    } else {
      $player1Arr = $updateRateArray[1];
      $player2Arr = $updateRateArray[0];
    }
    $setAllCount1 = intval($player1Arr['SETALL']);  //кол-во сетов всего первого
    $setAllCount2 = intval($player2Arr['SETALL']);//кол-во сетов всего второго
    $SetWinCount1 = intval($player1Arr['SETWIN']); //кол-во выигранных сетов первого
    $SetWinCount2 = intval($player2Arr['SETWIN']); //кол-во выигранных сетов второго 
    $matchCount1 = $player1Arr['MATCHES']; //Кол-во матчей первого
    $matchCount2 = $player2Arr['MATCHES']; //кол-во матчей второго 
    $Ra = $player1Arr['RATE'];//текущий рейтинг первого игрока
    $Rb = $player2Arr['RATE']; //текущий рейтинг второго игрока
    $expA = ($Rb-$Ra)/400;
    $Ea = 1/(1+pow(10,$expA));  //математическое ожидание A
    $expB = ($Ra-$Rb)/400;
    $Eb = 1/(1+pow(10,$expB));  //математическое ожидание B
    if ($scorePlayer1>$scorePlayer2) { //в случае победы первого игрока
      $newRa = $Ra + $N*(1-$Ea); //новый рейтинг игрока 1
      $newRb = $Rb + $N*(0-$Eb); //новый рейтинг игрока 2
    } else {  //победа игрока 2
      $newRa = $Ra + $N*(0-$Ea); //новый рейтинг игрока 1
      $newRb = $Rb + $N*(1-$Eb); //новый рейтинг игрока 2
    }

   //определение даты последнего матча
    $player1LastMatch = $matchDate;
    $player2LastMatch = $matchDate;
    if ($player1LastMatch < $player1Arr['LASTGAME']) {
        $player1LastMatch = $player1Arr['LASTGAME'];
    } 

    if ($player2LastMatch < $player2Arr['LASTGAME']) {
      $player2LastMatch = $player2Arr['LASTGAME'];
    } 
    
    // запись данных в матчи
    $sql = "INSERT INTO matches (`PLAYER 1 ID`, `PLAYER 2 ID`, `DATE`, `N`, `SCORE`, `PLAYER_1_RATE_BEFORE`, `PLAYER_2_RATE_BEFORE`, `PLAYER_1_RATE_AFTER`, `PLAYER_2_RATE_AFTER`) values ($player1ID, $player2ID,'$matchDate', $N, '$score', '$Ra', '$Rb', '$newRa', '$newRb')";    
    $result = $pdo->prepare($sql);
    $result->execute(); 

    // пишем статистику игрока №1
    $sql ="UPDATE players SET RATE = $newRa, MATCHES = $matchCount1 + 1, SETALL = $setAllCount1 + $setInMatch, SETWIN = $SetWinCount1 + $scorePlayer1, LASTGAME = '$player1LastMatch' WHERE ID=$player1ID";
    $resultUpdate = $pdo->prepare($sql);
    $resultUpdate->execute();

    // пишем статистику игрока №2
    $sql ="UPDATE players SET RATE = $newRb, MATCHES = $matchCount2 + 1, SETALL = $setAllCount2 + $setInMatch, SETWIN = $SetWinCount2 + $scorePlayer2, LASTGAME ='$player2LastMatch' WHERE ID=$player2ID";
    $resultUpdate = $pdo->prepare($sql);
    $resultUpdate->execute(); 

    // пишем результат на фронт
    $answerArr = array('status' => true, 'msg' => 'success bd', 'post' => $_POST);
    echo json_encode($answerArr);

}
?>