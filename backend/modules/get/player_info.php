<?
// Получение данных об игроке
if(isset($_GET['id'])) {
  
  $id = ($_GET['id']); //переданное id игрока
  if (ctype_digit($id)) {
   
    $sql ="SELECT `FIRSTNAME`, `LASTNAME`, `RATE`, `MATCHES`, `LASTGAME`, `SETWIN`, `SETALL`, `AVATAR` FROM players WHERE ID = $id";
    $result = $pdo->prepare($sql);
    $result->execute();
    $playerInfoArray = $result->fetchAll(); 
  
    if (!empty($playerInfoArray)) {
      // Матчи, где игрок - PLAYER 1 ID
      $sql ="SELECT players.FIRSTNAME, players.LASTNAME, matches.SCORE, matches.ID, matches.DATE, matches.`PLAYER 1 ID`, matches.`PLAYER 2 ID`, matches.PLAYER_1_RATE_BEFORE as RATE_BEFORE, matches.PLAYER_1_RATE_AFTER as RATE_AFTER FROM  matches JOIN players ON matches.`PLAYER 2 ID`=players.ID WHERE matches.`PLAYER 1 ID` = $id ORDER BY matches.DATE DESC";

      $result = $pdo->prepare($sql);
      $result->execute();
      $matchesArray1 = $result->fetchAll();



      //Матчи, где игрок - PLAYER 2 ID
      $sql ="SELECT players.FIRSTNAME, players.LASTNAME, matches.SCORE, matches.ID, matches.DATE, matches.`PLAYER 1 ID`, matches.`PLAYER 2 ID`, matches.PLAYER_2_RATE_BEFORE as RATE_BEFORE, matches.PLAYER_2_RATE_AFTER as RATE_AFTER FROM matches JOIN players ON matches.`PLAYER 1 ID`=players.ID WHERE matches.`PLAYER 2 ID` = $id ORDER BY matches.DATE DESC";

      $result = $pdo->prepare($sql);
      $result->execute();
      $matchesArray2 = $result->fetchAll();


      $resultMatchesArray = array_merge($matchesArray1, $matchesArray2);

      //сортировка массива по ['DATE']
      foreach ($resultMatchesArray as $key => $val) {
        $value[$key] = $val['DATE'];
      }
      array_multisort($value, SORT_DESC, $resultMatchesArray);

      $answerArr = array (
        'playerInfo'=> $playerInfoArray,
        'matches' => $resultMatchesArray,
      );
    } else {  
      $answerArr = array('status' => false, 'msg' => 'No player in DB with this ID');
    }
   } else {
      $answerArr = array('status' => false, 'msg' => 'GET-parameter is not integer');
      
   } 
   echo json_encode($answerArr);
} 
?>