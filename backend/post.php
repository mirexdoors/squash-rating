<? session_start();
//


require_once('bd_access.php');
include('modules/headers.php');
require_once('modules/db_pdo.php');

// запись результата матча
include('modules/post/match_add.php');

//регистрация игрока
include('modules/post/player_reg.php');


//вход пользователя
include('modules/post/player_login.php');

//изменение рег. статуса
include('modules/post/change_reg_status.php');

//удаление игрока
include('modules/post/player_delete.php');

//проверить статус пользователя (сессия)
include('modules/post/user_session.php');

//выход пользователя
include('modules/post/player_logout.php');


$typesArray = array(
"0"=>'login',
"1"=>'change_reg_status',
"2"=>'writescore',
"3"=>'delete',
"4"=>'reg',
"5" => 'user_status',
"6" => 'logout'
);

if (!$_POST['type']) {  
  $answerArr = array('status' => false, 'msg' => 'Expecting -type- in POST-request', 'post' => $_POST);
  echo json_encode($answerArr);
} elseif (in_array($_POST['type'], $typesArray)==false) {
  $answerArr = array('status' => false, 'msg' => 'Unexpected value of -type- in POST-request', 'post' => $_POST);
  echo json_encode($answerArr);
}
?>

