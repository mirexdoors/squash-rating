# Squash rating
## Rating app for squash league
Rating app for squash league. Based on Elo rating system.
Additional statistics:

* rate,
* match history,
* win/all sets.

## Site
http://rating.sibsquash.ru/
